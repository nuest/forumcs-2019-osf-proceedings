# ForumCS 2019 OSF Proceedings

This repository contains the workflow the create the conference proceedings of the [Forum Citizen Science 2019](https://www.buergerschaffenwissen.de/citizen-science/veranstaltungen/forum-citizen-science-2019).

The proceedings contain publicly reviewed articles.
The proceedings documents is published in the ForumCS OSF project at [https://osf.io/68uwn/](https://osf.io/68uwn/), where each article has a submodule with its own persistent identifier, a [DOI](https://en.wikipedia.org/wiki/Digital_object_identifier).

The whole workflow is scripted with [R](https://www.r-project.org/).
The main document is `proceedings.Rmd`.

The process currently is specific to the ForumCS, but could be abstracted if collaborators show up!
_Contributions welcome!_
Please note that the 'forumcs-2019-osf-proceedings' project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md).
By contributing to this project, you agree to abide by its terms.

## License

The template files and workflow are published under a Creative Commons Attribution 4.0 International License:
https://creativecommons.org/licenses/by/4.0/. If you are unsure what that means, the [tl;drLegal page](https://tldrlegal.com/license/creative-commons-attribution-4.0-international-(cc-by-4)) might help.
